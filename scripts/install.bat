@echo OFF

:: Ensure ADMIN privileges
:: Check for ADMIN Privileges
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
if '%errorlevel%' NEQ '0' (
	REM Get ADMIN Privileges
	echo Set UAC = CreateObject^("Shell.Application"^) >  "%temp%\getadmin.vbs"
	echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"
	"%temp%\getadmin.vbs"
	del "%temp%\getadmin.vbs"
	exit /B
) else (
	REM Got ADMIN Privileges
	pushd "%cd%"
	cd /d "%~dp0"
)

setlocal
:PROMPT
SET /P ACCEPTSOFTWARELICENSEAGREEMENT=Do you accept the EULA (to be found in doc/Licenses) (Y/[N])? 
IF /I "%ACCEPTSOFTWARELICENSEAGREEMENT%" NEQ "Y" GOTO ACCEPTSOFTWARELICENSEAGREEMENT_END

msg "%USERNAME%" Click Enter to continue or close the window to cancel...

pause

SET PATH_TO_FELIX=jserver-1.0.0
SET INSTALL_DIR_NAME=jserver-1.0.0

:: Old versions of jServer
:: SET OLD_INSTALL_DIR_NAME_v1-0-0=jserver-1.0.0

SET AUTOSTART_FILE_REL_PATH=windows
SET AUTOSTART_FILE=JS.xml
SET TASK_NAME="jServer"
SET SHORTCUT_PATH=%USERPROFILE%\Desktop
SET SHORTCUT_NAME=jServer
SET SHORTCUT_DESCRIPTION=jServer
SET SHORTCUT_VB_SCRIPT_NAME=jserver_shortcut
SET START_BAT=start.bat
SET SHORTCUT_ICON_PATH=windows
SET SHORTCUT_ICON=js_logo.ico

:: Check Program Files folders
:: if DEFINED PROGRAMFILES(X86) (
:: 	SET TOOL_INSTALL_DIR=%PROGRAMFILES(X86)%\%INSTALL_DIR_NAME%
:: ) else (
:: 	SET TOOL_INSTALL_DIR=%PROGRAMFILES%\%INSTALL_DIR_NAME%
:: )
SET TOOL_INSTALL_DIR=%PROGRAMFILES%\%INSTALL_DIR_NAME%

@echo ON
echo Removing old jServer versions
@echo OFF

:: Remove old versions from Program Files
:: IF EXIST "%PROGRAMFILES%\%OLD_INSTALL_DIR_NAME_v1-0-0%" (
:: rmdir /s /q "%PROGRAMFILES%\%OLD_INSTALL_DIR_NAME_v1-0-0%"
:: )

:: Remove same version from Program Files
IF EXIST "%TOOL_INSTALL_DIR%" (
rmdir /s /q "%TOOL_INSTALL_DIR%"
)

:: Remove shortcut
IF EXIST "%SHORTCUT_PATH%\%SHORTCUT_NAME%.lnk" (
del "%SHORTCUT_PATH%\%SHORTCUT_NAME%.lnk"
)

@echo ON
echo Done removing old jServer versions
echo Installing jServer
@echo OFF

:: Copy felix to Program Files
MKDIR "%TOOL_INSTALL_DIR%"
XCOPY "%PATH_TO_FELIX%" "%TOOL_INSTALL_DIR%" /E /C /R /I /K /Y

:: Check installation
IF NOT EXIST "%TOOL_INSTALL_DIR%" GOTO INSTALLATIONFAILED

:: Change user permissions
icacls "%TOOL_INSTALL_DIR%" /grant %USERNAME%:(F,MA) /T /C
:: Create task for autostart
schtasks /CREATE /TN %TASK_NAME% /F /XML "%TOOL_INSTALL_DIR%\%AUTOSTART_FILE_REL_PATH%\%AUTOSTART_FILE%"
:: Create shortcut
cd %SHORTCUT_PATH%
if exist %SHORTCUT_VB_SCRIPT_NAME%.vbs del %SHORTCUT_VB_SCRIPT_NAME%.vbs
FOR /F "tokens=1* delims=;" %%B IN ("Set oWS = WScript.CreateObject("WScript.Shell")") do echo %%B>>%SHORTCUT_PATH%\%SHORTCUT_VB_SCRIPT_NAME%.vbs   
FOR /F "tokens=1* delims=;" %%B IN ("sLinkFile = "%SHORTCUT_PATH%\%SHORTCUT_NAME%.lnk"") do echo %%B>>%SHORTCUT_PATH%\%SHORTCUT_VB_SCRIPT_NAME%.vbs
FOR /F "tokens=1* delims=;" %%B IN ("Set oLink = oWS.CreateShortcut(sLinkFile)") do echo %%B>>%SHORTCUT_PATH%\%SHORTCUT_VB_SCRIPT_NAME%.vbs
FOR /F "tokens=1* delims=;" %%B IN ("   oLink.TargetPath = "%TOOL_INSTALL_DIR%\%START_BAT%"") do echo %%B>>%SHORTCUT_PATH%\%SHORTCUT_VB_SCRIPT_NAME%.vbs
FOR /F "tokens=1* delims=;" %%B IN ("   oLink.Description = "%SHORTCUT_DESCRIPTION%"") do echo %%B>>%SHORTCUT_PATH%\%SHORTCUT_VB_SCRIPT_NAME%.vbs
FOR /F "tokens=1* delims=;" %%B IN ("   oLink.WorkingDirectory = "%TOOL_INSTALL_DIR%"") do echo %%B>>%SHORTCUT_PATH%\%SHORTCUT_VB_SCRIPT_NAME%.vbs
FOR /F "tokens=1* delims=;" %%B IN ("   oLink.IconLocation = "%TOOL_INSTALL_DIR%\%SHORTCUT_ICON_PATH%\%SHORTCUT_ICON%"") do echo %%B>>%SHORTCUT_PATH%\%SHORTCUT_VB_SCRIPT_NAME%.vbs
FOR /F "tokens=1* delims=;" %%B IN ("   oLink.Save") do echo %%B>>%SHORTCUT_PATH%\%SHORTCUT_VB_SCRIPT_NAME%.vbs
CSCRIPT %SHORTCUT_VB_SCRIPT_NAME%.vbs
del %SHORTCUT_VB_SCRIPT_NAME%.vbs

powercfg -x -standby-timeout-ac 0
powercfg -x -standby-timeout-dc 0
powercfg -x -hibernate-timeout-ac 0
powercfg -x -hibernate-timeout-dc 0

:: User output
IF EXIST "%SHORTCUT_PATH%\%SHORTCUT_NAME%.lnk" (
msg "%USERNAME%" Successfully installed jServer. To start the software doubleclick the link at '%SHORTCUT_PATH%'. To start the software automatically log out and log in again or restart the computer.
) ELSE (
msg "%USERNAME%" Successfully installed jServer. To start the software automatically log out and log in again or restart the computer.
)
Exit /B 5

:INSTALLATIONFAILED
endlocal
:: User output
msg "%USERNAME%" Could not install jServer. Please try again.
Exit /B 5

:ACCEPTSOFTWARELICENSEAGREEMENT_END
endlocal
:: User output
msg "%USERNAME%" You have to accept the EULA (to be found in doc/Licenses) to install and use the software...
Exit /B 5
