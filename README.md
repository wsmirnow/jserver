jServer
=====
A log and version server.

Copyright (c) 2015 Denis Meyer

Version
-----
1.0.0

Author
-----
- Denis Meyer ( calltopower88 [at] gmail [dot] com )

3rd-party licenses
-----
- Apache Felix licensed under the Apache License 2.0 ( http://felix.apache.org/license.html )

Requirements
-----
- Oracle Java 8 JRE

Default port
-----
In "config/config.properties":
- Default: 8088
