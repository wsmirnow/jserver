/*
 * Copyright (c) 2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.jserver.lock;

/**
 * Constants
 *
 * @date 21.02.2015
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
public class Constants {

    public final static String JLS_LOCK_FILE_NAME = "__JLS_lock-file";
    public final static String JLS_LOCK_FILE_LOCATION = "user.home"; // has to be a system property
    public final static String JLS_LOCK_FILE_SUFFIX = ".tmp";
    // strings
    public final static String str_instanceRunning_title = "Another instance is already running.\nPlease quit the running application or end it via the task manager.";
    public final static String str_instanceRunning_msg = "Another instance is already running";
    public final static String str_quit = "Quit";
}
