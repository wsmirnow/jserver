/*
 * Copyright (c) 2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.jserver.lock;

import javax.swing.JOptionPane;
import org.apache.log4j.Logger;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Activator
 *
 * @date 21.02.2015
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
public class Activator implements BundleActivator {

    private static final Logger logger = Logger.getLogger(Activator.class);
    private BundleContext bc;

    @Override
    public void start(BundleContext _bc) throws Exception {
        this.bc = _bc;

        Lock lock = new Lock(Constants.JLS_LOCK_FILE_NAME);
        if (lock.otherInstanceIsRunning()) {
            logger.error("Activator::start - Another instance is running. Quitting...\n####################");
            Object[] options = {
                Constants.str_quit
            };
            int n = JOptionPane.showOptionDialog(null,
                    Constants.str_instanceRunning_msg,
                    Constants.str_instanceRunning_title,
                    JOptionPane.YES_OPTION,
                    JOptionPane.ERROR_MESSAGE,
                    null,
                    options,
                    options[0]);
            if (n == JOptionPane.YES_OPTION) {
                if (bc != null) {
                    if (logger.isInfoEnabled()) {
                        logger.info("Activator::start - Getting system bundle...");
                    }
                    Bundle b = bc.getBundle(0);
                    if (b != null) {
                        if (logger.isInfoEnabled()) {
                            logger.info("Activator::start - Stopping felix...");
                        }
                        b.stop();
                    } else {
                        logger.error("Activator::start - Could not stop felix...");
                    }
                } else {
                    logger.error("Activator::start - Could not get system bundle...");
                }
                Runtime.getRuntime().halt(0);
            }
        }
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        if (logger.isInfoEnabled()) {
            logger.info("Activator::stop - Stopping bundle...");
        }
    }
}
