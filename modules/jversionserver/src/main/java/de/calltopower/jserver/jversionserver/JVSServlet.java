/*
 * Copyright (c) 2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.jserver.jversionserver;

import de.calltopower.jserver.jversionserver.conf.Configuration;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.log4j.Logger;

/**
 * JLSServlet
 *
 * @date 21.02.2015
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
public class JVSServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(JVSServlet.class);
    private final Configuration config;
    private String JVS_API_VERSION = "1.0";
    private String JVS_PATH_TO_VERSION_FILE = "versions.db";
    private String JVS_POST_APPNAME = "unknown";

    public JVSServlet(Configuration _config) {
        this.config = _config;
        readProperties();
        log();
    }

    private void readProperties() {
        if (logger.isInfoEnabled()) {
            logger.info("JLSServlet::readProperties");
        }
        JVS_API_VERSION = config.get(Constants.PROPKEY_JVS_API_VERSION);
        JVS_PATH_TO_VERSION_FILE = config.get(Constants.PROPKEY_VERSION_FILE_PATH);
        JVS_POST_APPNAME = config.get(Constants.PROPKEY_POST_APPNAME).toLowerCase();
    }

    public final void log() {
        if (logger.isInfoEnabled()) {
            logger.info("--------------------------------------------------");
            logger.info("Versions file: " + JVS_PATH_TO_VERSION_FILE);
            logger.info("POST AppName: " + JVS_POST_APPNAME);
            logger.info("--------------------------------------------------");
        }
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        if (logger.isInfoEnabled()) {
            logger.info("JVSServlet::init");
        }
    }

    @Override
    public void destroy() {
        if (logger.isInfoEnabled()) {
            logger.info("JVSServlet::destroy");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            if (logger.isInfoEnabled()) {
                logger.info("JVSServlet::doGet - GET request recieved");
            }

            resp.setContentType("text/html");

            // return early if this is a HEAD request
            if (req.getMethod().equals("HEAD")) {
                return;
            }

            PrintWriter out = resp.getWriter();

            Date date = new Date();
            DateFormat df = DateFormat.getInstance();

            String zone = req.getParameter("zone");
            if (zone != null) {
                TimeZone tz = TimeZone.getTimeZone(zone);
                df.setTimeZone(tz);
            }

            out.println("<html>");
            out.println("<head><title>jVersionServer</title></head>");
            out.println("<body>");
            out.println(df.format(date) + "<br />");
            out.println("jVersionServer is running.<br />");
            out.println("Version " + Constants.JVS_VERSION + " Build " + Constants.JVS_BUILD + "<br />");
            out.println("API Version " + JVS_API_VERSION + "<br /><br />");
            out.println("This server is powered by " + req.getServerName());
            out.println("</body></html>");
        } catch (Exception ex) {
            logger.error("JVSServlet::doGet - Exception: " + ex.getMessage());
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        try {
            if (logger.isInfoEnabled()) {
                logger.info("JVSServlet::doPost - POST request recieved");
            }

            resp.setContentType("text/plain");

            boolean isMultipart = ServletFileUpload.isMultipartContent(req);
            if (isMultipart) {
                if (logger.isInfoEnabled()) {
                    logger.info("JVSServlet::doPost - Is multipart");
                }
                try {
                    ServletFileUpload upload = new ServletFileUpload();

                    String appName = null;

                    FileItemIterator iter = upload.getItemIterator(req);
                    while (iter.hasNext()) {
                        FileItemStream item = iter.next();
                        String name = item.getFieldName();
                        InputStream stream = item.openStream();
                        if (item.isFormField()) {
                            if (name.equalsIgnoreCase(JVS_POST_APPNAME)) {
                                appName = Streams.asString(stream);
                            } else {
                                String val = Streams.asString(stream);
                                logger.error("JVSServlet::handleMultipart - Unknown form field: " + name + ", value: " + val);
                            }
                        } else {
                            logger.error("JLSServlet::handleMultipart - Unknown field: " + name + ", file name: " + item.getName());
                        }
                    }
                    if ((appName != null) && !appName.isEmpty()) {
                        appName = appName.toLowerCase();
                        JVSVersionDatabase jvdb = new JVSVersionDatabase();
                        jvdb.loadDatabase(JVS_PATH_TO_VERSION_FILE);
                        Map<String, String> map = jvdb.getMap();
                        if (!map.isEmpty()) {
                            if (map.containsKey(appName)) {
                                out.println(map.get(appName));
                                resp.setStatus(HttpServletResponse.SC_OK);
                            } else {
                                logger.error("JLSServlet::handleMultipart - No version available for " + appName);
                                out.println("No version available for " + appName);
                                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                            }
                        } else {
                            logger.error("JLSServlet::handleMultipart - No version available for " + appName);
                            out.println("No version available for " + appName);
                            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                        }
                    } else {
                        logger.error("JLSServlet::handleMultipart - No application name parameter given");
                        out.println("No application name parameter given");
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }
                } catch (FileUploadException ex) {
                    logger.error("JLSServlet::handleMultipart - FileUploadException: " + ex.getMessage());
                    out.println("FileUploadException");
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                }
            } else {
                logger.error("JVSServlet::doPost - Is not multipart");
                out.println("Is not multipart");
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        } catch (IOException ex) {
            logger.error("JVSServlet::doPost - IOException: " + ex.getLocalizedMessage());
            out.println("Internal server error");
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (Exception ex) {
            logger.error("JVSServlet::doPost - Exception: " + ex.getLocalizedMessage());
            out.println("Internal server error");
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } finally {
            out.flush();
            out.close();
        }
    }
}
