/*
 * Copyright (c) 2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.jserver.jversionserver;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * JVSVersionDatabase
 *
 * @date 21.02.2015
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
public class JVSVersionDatabase {

    private static final Logger logger = Logger.getLogger(JVSVersionDatabase.class);
    private Map<String, String> map = null;

    public JVSVersionDatabase() {
        map = new HashMap<>();
    }

    public Map<String, String> getMap() {
        return map;
    }

    public void loadDatabase(String databasePath) throws IOException {
        if (map != null) {
            map.clear();
        }
        map = new HashMap<>();
        InputStream is = null;
        try {
            File f = new File(databasePath);
            if (!f.exists()) {
                if (!f.createNewFile()) {
                    logger.error("JVSVersionDatabase::loadDatabase - Cannot create version file: " + databasePath);
                    throw new IOException("JVSVersionDatabase::loadDatabase - Cannot create version file: " + databasePath);
                } else {
                    if (logger.isInfoEnabled()) {
                        logger.info("JVSVersionDatabase::loadDatabase - Created empty version file at: " + databasePath);
                    }
                }
            }
            if (f.isFile() && f.canRead()) {
                if (logger.isInfoEnabled()) {
                    logger.info("JVSVersionDatabase::loadDatabase - Found version file at: " + databasePath);
                }
                BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
                String line;
                while ((line = in.readLine()) != null) {
                    if (!line.isEmpty()) {
                        if (line.contains("=")) {
                            String[] split = line.split("=");
                            if (split.length == 2) {
                                map.put(split[0].toLowerCase(), split[1]);
                            }
                        }
                    }
                }
            } else {
                logger.error("JVSVersionDatabase::loadDatabase - Cannot read file or is not a file: " + databasePath);
                throw new IOException("JVSVersionDatabase::loadDatabase - Cannot read file or is not a file: " + databasePath);
            }
        } catch (IOException ex) {
            logger.error("JVSVersionDatabase::loadDatabase - Failed to load version file: " + ex.getMessage());
            throw ex;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    logger.error("JLSKeyDatabase::loadDatabase - Failed to close stream: " + ex.getMessage());
                    logger.error("JLSKeyDatabase::loadDatabase - Failed to close stream: " + ex.getMessage());
                    throw ex;
                }
            }
        }
    }
}
