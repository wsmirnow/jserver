/*
 * Copyright (c) 2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.jserver.jversionserver;

/**
 * Constants
 *
 * @date 21.02.2015
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
public class Constants {

    public final static String JVS_VERSION = "1.0.0";
    public final static String JVS_BUILD = "20.03.2015 1";
    // misc
    public final static String str_versionPath = "/version/";
    // config
    public static final String DEFAULT_CONFIG_PATH = "/config/default.properties";
    public static final String CONFIG_PATH = "./conf/jvs.properties";
    // API version
    public final static String PROPKEY_JVS_API_VERSION = "de.calltopower.jserver.jversionserver.api.version";
    // file system
    public static final String PROPKEY_VERSION_FILE_PATH = "de.calltopower.jserver.jversionserver.versionfile";
    public static final String PROPKEY_POST_APPNAME = "de.calltopower.jserver.jversionserver.post.name";
}
