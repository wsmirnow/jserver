/*
 * Copyright (c) 2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.jserver.gui;

import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Activator
 *
 * @date 21.02.2015
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
public class Activator implements BundleActivator {

    // misc
    private static final Logger logger = Logger.getLogger(Activator.class);
    private BundleContext bc;
    private TrayIcon trayIcon;
    private Image trayIconImage;
    private final ActionListener quitApplicationListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            SystemTray.getSystemTray().remove(trayIcon);
            Runtime.getRuntime().halt(0);
        }
    };

    @Override
    public void start(BundleContext _bc) throws Exception {
        this.bc = _bc;

        trayIconImage = ImageIO.read(getClass().getResourceAsStream(Constants.trayIconImagePath));
        // initialize tray icon
        if (SystemTray.isSupported()) {
            // build menu
            MenuItem JSUIItem = new MenuItem(Constants.str_name);
            JSUIItem.setEnabled(false);
            MenuItem showQuitItem = new MenuItem(Constants.str_quit);
            showQuitItem.addActionListener(quitApplicationListener);

            PopupMenu mainMenu = new PopupMenu();
            mainMenu.add(JSUIItem);
            mainMenu.addSeparator();
            mainMenu.add(showQuitItem);

            // build tray icon
            trayIcon = new TrayIcon(trayIconImage, Constants.str_name, mainMenu);
            trayIcon.setImageAutoSize(true);
            SystemTray tray = SystemTray.getSystemTray();
            tray.add(trayIcon);
        } else {
            logger.error("DesktopUI::init - Could not initialize tray icon. Not supported for this operating system.");
            JOptionPane.showMessageDialog(null,
                    Constants.str_trayIconNotInit_msg,
                    Constants.str_trayIconNotInit_title,
                    JOptionPane.ERROR_MESSAGE);
            if (bc != null) {
                if (logger.isInfoEnabled()) {
                    logger.info("Activator::start - Stopping module...");
                }
                this.stop(bc);
            } else {
                logger.error("Activator::start - Could not get bundle context...");
            }
        }
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        SystemTray.getSystemTray().remove(trayIcon);
    }
}
