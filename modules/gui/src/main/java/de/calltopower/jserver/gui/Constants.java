/*
 * Copyright (c) 2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.jserver.gui;

/**
 * Constants
 *
 * @date 21.02.2015
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
public class Constants {

    // resources
    public final static String trayIconImagePath = "/ui/matterhorn-icon.png";
    // strings
    public final static String str_name = "jServer";
    public final static String str_quit = "Quit";
    public final static String str_trayIconNotInit_title = "Could not initialize tray icon";
    public final static String str_trayIconNotInit_msg = "Could not initialize tray icon. Not supported for this operating system.";
}
